package Persistencia;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import javax.swing.JOptionPane;


class MySQLPropertiesFile {
    
    public static Properties properties = null;
    
    public MySQLPropertiesFile(){
        super();
    }
    
    private static Properties getProperties(){
        if(properties == null){
            properties = new Properties();
            try{
                properties.load(properties.getClass().getResourceAsStream("/src/persistencia/mysql.properties"));
            } catch(FileNotFoundException e){
                e.printStackTrace();
            } catch(IOException e){
                e.printStackTrace();
            }
        }
        return properties;
    }
    
    public static String getProperty(String nameProperty){
        return getProperties().getProperty(nameProperty);
    }
}


/**
 *
 * @author ING JARC
 */
public class ConexionBD {
    public String puerto = "3306";
    public String nomservidor = "localhost";
    public String db = "databasename";
    public String user = "root";
    public String pass = "";
    
    private Connection conn = null;
    private Statement statement;
    private MySQLPropertiesFile properties = new MySQLPropertiesFile();
    
    public ConexionBD(){
        super();
        this.puerto = this.properties.getProperty("port");
        this.nomservidor = this.properties.getProperty("server.name");
        this.db = this.properties.getProperty("database.name");
        this.user = this.properties.getProperty("database.user");
        this.pass = this.properties.getProperty("database.pass");
    }

    public Connection conectar(){
        try {
            String ruta = "jdbc:mysql://";
            String servidor = nomservidor + ":" + puerto + "/";
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(ruta + servidor + db, user, pass);
            if (conn != null) {
                System.out.println("Conexión a base de datos listo...");
                this.statement = this.conn.createStatement();
                ResultSet res = this.statement.executeQuery("select name from Administrator where idAdministrator='1'");
                res.next();
                System.out.println(res.getString("name"));
            } else if (conn == null) {
                throw new SQLException();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, e.getMessage());
        } catch (ClassNotFoundException e) {
            JOptionPane.showMessageDialog(null, "Se produjo el siguiente error: " + e.getMessage());
        } catch (NullPointerException e) {
            JOptionPane.showMessageDialog(null, "Se produjo el siguiente error: " + e.getMessage());
        } finally {
            return conn;
        }
    }

    public void desconectar() {
        conn = null;
        System.out.println("Desconexion a base de datos listo...");
    }
    /*
    public static void main(String args[]){
        ConexionBD c = new ConexionBD();
        c.conectar();
    }*/
}
